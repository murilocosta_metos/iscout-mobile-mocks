<?php

define("BASE_URL", 'https://murilo-mocks.weiz.metos.at');

return [
    [
        'method' => 'POST',
        'url' => BASE_URL . '/user/mobile/register',
        'path' => '/user/mobile/register',
        'description' => 'Register a new user',
        'params' => false,
        'body' => [
            'username' => 'mockUser',
            'password' => 'mockPassword',
            'info' => [
                'name' => 'Mock',
                'lastname' => 'User',
                'email' => 'mock.user@test.com',
            ],
            'settings' => [
                'language' => 'de',
            ],
            'address' => [
                'country' => 'AT',
            ],
        ],
        'resp' => false,
    ],
    [
        'method' => 'GET',
        'url' => BASE_URL . '/dev/user/activate/94a08da1fecbb6e8b46990538c7b50b2',
        'path' => '/dev/user/activate/@accountActivationToken',
        'description' => 'Activate an user account with the token sent by email',
        'params' => [
            'accountActivationToken' => '94a08da1fecbb6e8b46990538c7b50b2',
        ],
        'body' => false,
        'resp' => false,
    ],
    [
        'method' => 'POST',
        'url' => BASE_URL . '/dev/user/activate-account',
        'description' => 'Activate an user account with the token sent by email',
        'path' => '/dev/user/activate-account',
        'params' => false,
        'body' => [
            'token' => '94a08da1fecbb6e8b46990538c7b50b2',
        ],
        'resp' => false,
    ],
    [
        'method' => 'POST',
        'url' => BASE_URL . '/user/mobile/password-reset',
        'path' => '/user/mobile/password-reset',
        'description' => 'Request a password reset',
        'params' => false,
        'body' => [
            'username' => 'mockUser',
        ],
        'resp' => false,
    ],
    [
        'method' => 'POST',
        'url' => BASE_URL . '/dev/user/IScoutMobile/password-update/94a08da1fecbb6e8b46990538c7b50b2',
        'path' => '/dev/user/@oAuthClientID/password-update/@passwordChangeToken',
        'description' => "Update an user's password using a token sent by email",
        'params' => [
            'oAuthClientID' => 'IScoutMobile',
            'passwordChangeToken' => '94a08da1fecbb6e8b46990538c7b50b2',
        ],
        'body' => [
            'password' => 'newMockPassword',
            'password_repeat' => 'newMockPassword',
        ],
        'resp' => false,
    ],
    [
        'method' => 'POST',
        'url' => BASE_URL . '/camera/userTraps',
        'path' => '/camera/userTraps',
        'description' => "Create a new user's trap by name and/or position",
        'params' => false,
        'body' => [
            'name' => 'NewMockTrap',
            'position' => [
                'geo' => [
                    'coordinates' => [
                        '47.070713',
                        '15.439504',
                    ],
                ],
                'altitude' => '353',
                'timezoneCode' => 'Europe/Vienna',
            ],
        ],
        'resp' => [
            'stationID' => 'D0100001',
        ],
    ],
    [
        'method' => 'PUT',
        'url' => BASE_URL . '/camera/userTraps/D0100001',
        'path' => '/camera/userTraps/@userTrapID',
        'description' => "Update a user trap's name and/or position",
        'params' => [
            'userTrapID' => 'D0100001',
        ],
        'body' => [
            'name' => 'UpdatedMockTrap',
            'position' => [
                'geo' => [
                    'coordinates' => [
                        '47.217210',
                        '15.623044',
                    ],
                ],
                'altitude' => '353',
                'timezoneCode' => 'Europe/Vienna',
            ],
        ],
        'resp' => false,
    ],
    [
        'method' => 'GET',
        'url' => BASE_URL . '/camera/userTraps?page=1',
        'path' => '/camera/userTraps?page=@page',
        'description' => "Fetch a list of user trap's summary (maximal 3 traps per request)",
        'params' => [
            'page' => 1,
        ],
        'body' => false,
        'resp' => [
            'lastPicture' => [
                'time' => '2021-11-22 10:01:46',
                'photo_time' => '2021-11-22 10:01:46',
                'nm' => 'D0100001',
                'cam_id' => 1,
                'pic_id' => 1943,
                'filename' => '20211122_1001_072053B8_PIC_1943_CAM_1.xml.pi',
                'rectangles' => [[
                    'origin' => 'detection',
                    'rect_id' => '4ecb25ca13412',
                    'generated_by' => 'system',
                    'label' => 'Lepidoptera',
                    'probability' => 0.9951707720756531,
                    'x_max' => 0.3912622630596161,
                    'x_min' => 0.3486573100090027,
                    'y_max' => 0.47797396779060364,
                    'y_min' => 0.4301004707813263,
                    'new' => true,
                    'attempt' => 1,
                    'hidden' => false,
                ]],
            ],
            'recentData' => [
                [
                    'detections' => 5,
                    'nm' => 'D0100001',
                    'label' => 'Cydia pomonella',
                    'detection_time' => '2021-09-13',
                ],
                [
                    'detections' => 4,
                    'nm' => 'D0100001',
                    'label' => 'Cydia pomonella',
                    'detection_time' => '2021-09-12',
                ],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => BASE_URL . '/camera/D0100001/photos/from/1637571706/to/1635846527',
        'path' => '/camera/@userTrapID/photos/from/@fromTimestamp/to/@toTimestamp',
        'description' => "Fetch a list of photos from a specific trap",
        'params' => [
            'userTrapID' => 'D0100001',
            'fromTimestamp' => '1637571706',
            'toTimestamp' => '1635846527',
        ],
        'body' => false,
        'resp' => [
            [
                'time' => '2021-11-22 10:01:46',
                'photo_time' => '2021-11-22 10:01:46',
                'nm' => 'D0100001',
                'cam_id' => 1,
                'pic_id' => 1943,
                'filename' => '20211122_1001_072053B8_PIC_1943_CAM_1.xml.pi',
                'rectangles' => [[
                    'origin' => 'detection',
                    'rect_id' => '4ecb25ca13412',
                    'generated_by' => 'system',
                    'label' => 'Lepidoptera',
                    'probability' => 0.9951707720756531,
                    'x_max' => 0.3912622630596161,
                    'x_min' => 0.3486573100090027,
                    'y_max' => 0.47797396779060364,
                    'y_min' => 0.4301004707813263,
                    'new' => true,
                    'attempt' => 1,
                    'hidden' => false,
                ]],
            ],
        ],
    ],
    [
        'method' => 'GET',
        'url' => BASE_URL . '/camera/D0100001/monitoring/iscout?type=interval&from=1637571706&to=1635846527',
        'path' => '/camera/@userTrapID/monitoring/iscout?type=@filterType&from=@fromTimestamp&to=@toTimestamp',
        'description' => "Fetch a trap's monitoring data from a specific period of time",
        'params' => [
            'userTrapID' => 'D0100001',
            'filterType' => 'interval',
            'fromTimestamp' => '1637571706',
            'toTimestamp' => '1635846527',
        ],
        'body' => false,
        'resp' => [
            [
                'detections' => 5,
                'nm' => 'D0100001',
                'label' => 'Cydia pomonella',
                'detection_time' => '2021-09-13',
            ],
            [
                'detections' => 4,
                'nm' => 'D0100001',
                'label' => 'Cydia pomonella',
                'detection_time' => '2021-09-12',
            ]
        ],
    ],
    [
        'method' => 'POST',
        'url' => BASE_URL . '/camera/D0100001/detector/insects',
        'path' => '/camera/@userTrapID/detector/@detectorType',
        'description' => "Upload an image to the detector service and receive the detections",
        'params' => [
            'userTrapID' => 'D0100001',
            'detectorType' => 'insects',
        ],
        'body' => [
            'metosImage' => '/tmp/photos/example-picture.jpg',
            'rows' => 2,
            'cols' => 2,
        ],
        'resp' => [
            [
                'generated_by' => 'system',
                'label' => 'Lepidoptera',
                'probability' => 0.9951707720756531,
                'x_max' => 0.3912622630596161,
                'x_min' => 0.3486573100090027,
                'y_max' => 0.47797396779060364,
                'y_min' => 0.4301004707813263
            ],
        ],
    ],
];
