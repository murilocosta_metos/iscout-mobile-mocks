<?php

require_once 'vendor/autoload.php';

define("SCHEMAS_PATH", __DIR__ . '/schemas/');
define("RESOURCES_PATH", __DIR__ . '/resources/');
define("UPLOADS_PATH", __DIR__ . '/uploads/');

use flight\Engine;
use Mocks\{FileUploader, JsonValidator, ResourceReader};
use Monolog\{Handler\StreamHandler, Logger};

Flight::register('logger', Logger::class, ['mocks'], function (Logger $log) {
    $logHandler = new StreamHandler('./mocks.log');
    $log->pushHandler($logHandler);
});

$app = new Engine();

$app->route('GET /', function () {
    $availableEndpoints = require_once __DIR__ . '/routes.php';
	Flight::render('home', [
        'availableEndpoints' => $availableEndpoints
    ]);
});

$app->route('POST /user/mobile/register', function () {
    $payload = json_decode(Flight::request()->getBody());
    $response = [
        'content' => ['message' => 'User registered'],
        'status' => 200
    ];

    $validator = new JsonValidator('user-mobile-register');
    if (!$validator->validate($payload)) {
        $response['content'] = $validator->getErrors();
        $response['status'] = 400;
    }

    Flight::json($response['content'], $response['status']);
});

$app->route('POST /user/mobile/password-reset', function () {
    $username = Flight::request()->data['username'];
    Flight::logger()->info('Password reset request sent', ['username' => $username]);

    Flight::json(['message' => 'Password reset request sent'], 200);
});

$app->route('GET /dev/user/activate/@accountActivationToken', function ($accountActivationToken) {
    Flight::logger()->info('User account activated', [
        'token' => $accountActivationToken
    ]);

    Flight::json(['message' => 'User account activated'], 200);
});

$app->route('POST /dev/user/activate-account', function () {
    Flight::logger()->info('User account activated', [
        'token' => Flight::request()->data['token']
    ]);

    Flight::json(['message' => 'User account activated'], 200);
});

$app->route('POST /dev/user/@oAuthClientID/password-update/@passwordChangeToken', function (
    $oAuthClientID,
    $passwordChangeToken
) {
    Flight::logger()->info('User password updated', [
        'clientID' => $oAuthClientID,
        'token' => $passwordChangeToken
    ]);

    $payload = json_decode(Flight::request()->getBody());
    $response = [
        'content' => ['message' => 'User password updated'],
        'status' => 200
    ];

    $validator = new JsonValidator('dev-user-password-update');
    if (!$validator->validate($payload)) {
        $response['content'] = $validator->getErrors();
        $response['status'] = 400;
    }

    Flight::json($response['content'], $response['status']);
});

$app->route('POST /camera/userTraps', function () {
    $payload = json_decode(Flight::request()->getBody());
    $response = [
        'content' => ['message' => 'User insect trap created'],
        'status' => 200
    ];

    $validator = new JsonValidator('camera-userTraps');
    if (!$validator->validate($payload)) {
        $response['content'] = $validator->getErrors();
        $response['status'] = 400;
    }

    Flight::json($response['content'], $response['status']);
});

$app->route('PUT /camera/userTraps/@userTrapID', function ($userTrapID) {
    $payload = json_decode(Flight::request()->getBody());
    $response = [
        'content' => ['message' => 'User insect trap updated'],
        'status' => 200
    ];

    $validator = new JsonValidator('camera-userTraps');

    if (!$validator->validate($payload)) {
        $response['content'] = $validator->getErrors();
        $response['status'] = 400;
    }

    Flight::json($response['content'], $response['status']);
});

$app->route('GET /camera/userTraps', function () {
    Flight::logger()->info('User insect traps summary requested');

    $payload = ResourceReader::get('trap-summary');
    Flight::json($payload);
});

$app->route('POST /camera/@userTrapID/photos', function ($userTrapID) {
    $uploadedFile = Flight::request()->files['image'];
    FileUploader::upload($uploadedFile);
    Flight::json(['message' => 'Photo upload completed']);
});

$app->route('GET /camera/@userTrapID/photos/from/@fromTimestamp/to/@toTimestamp', function (
    $userTrapID,
    $fromTimestamp,
    $toTimestamp
) {
    Flight::logger()->info('User insect traps gallery requested', [
        'trapID' => $userTrapID,
        'from' => $fromTimestamp,
        'to' => $toTimestamp
    ]);

    $payload = ResourceReader::get('trap-photo-gallery');
    Flight::json($payload);
});

$app->route('GET /camera/@userTrapID/monitoring/iscout', function ($userTrapID) {
    Flight::logger()->info('User insect traps monitoring data requested', [
        'trapID' => $userTrapID,
        'type' => Flight::request()->query['type'],
        'from' => Flight::request()->query['from'],
        'to' => Flight::request()->query['to']
    ]);

    $payload = ResourceReader::get('trap-monitoring-data');
    Flight::json($payload);
});

$app->route('POST /camera/@userTrapID/detector/@detectorType', function ($userTrapID, $detectorType) {
    Flight::logger()->info('Insects detection was requested', [
        'trapID' => $userTrapID,
        'detectorType' => $detectorType
    ]);

    $uploadedFile = Flight::request()->files['metosImage'];
    if ($uploadedFile !== null) {
        FileUploader::upload($uploadedFile);
    }

    $payload = ResourceReader::get('trap-detection');
    Flight::json($payload);
});

$app->start();
