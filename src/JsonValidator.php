<?php

namespace Mocks;

use JsonSchema\Validator;

class JsonValidator
{
    private $schemaURL;
    private $instance;
    private $errors;

    public function __construct($jsonSchemaName)
    {
        $jsonSchemaPath = 'file://' . SCHEMAS_PATH . "$jsonSchemaName.json";
        $jsonSchemaContent = file_get_contents($jsonSchemaPath);
        $this->schemaURL = json_decode($jsonSchemaContent);
        $this->instance = new Validator();
    }

    public function validate($jsonData): bool
    {
        $this->instance->validate($jsonData, $this->schemaURL);

        if (!$this->instance->isValid()) {
            foreach ($this->instance->getErrors() as $error) {
                $this->errors[$error['property']] = $error['message'];
            }
            return false;
        }

        return true;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
