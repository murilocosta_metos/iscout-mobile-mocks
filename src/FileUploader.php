<?php

namespace Mocks;

class FileUploader
{
    public static function upload($uploadedFile): void
    {
        [$fileName, $fileExtension] = explode('.', $uploadedFile['name']);
        $targetFile = UPLOADS_PATH . md5($fileName) . ".$fileExtension";
        move_uploaded_file($uploadedFile['tmp_name'], $targetFile);
    }
}
