<?php

namespace Mocks;

class ResourceReader
{
    public static function get($resourcePath)
    {
        $resourceContent = file_get_contents(RESOURCES_PATH . "$resourcePath.json");
        return json_decode($resourceContent, true);
    }
}