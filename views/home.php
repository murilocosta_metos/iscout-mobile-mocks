<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>iScout Mobile Mocks</title>
    <meta name="description" content="A simple API mock project.">
    <meta name="author" content="METOS">

    <meta property="og:title" content="iScout Mobile Mocks">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://murilo-mocks.weiz.metos.at">
    <meta property="og:description" content="A simple API mock project.">
    <meta property="og:image" content="image.png">

    <link rel="stylesheet" href="views/bootstrap.min.css">
    <script src="views/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h1 class="mt-5">Welcome to the API mocks</h1>
    <p class="lead">This is a simple website to mock API calls.</p>

    <section>
        <?php foreach ($availableEndpoints as $endpoint): ?>
        <div class="list-group" style="margin-bottom: 30px">
            <div class="list-group-item d-flex justify-content-between align-items-start">
                <div>
                    <span class="badge <?php echo $endpoint['method'] === 'GET' ? 'bg-success' : ($endpoint['method'] === 'POST' ? 'bg-primary' : ($endpoint['method'] === 'DELETE' ? 'bg-danger' : 'bg-info')) ?>">
                        <?php echo $endpoint['method'] ?>
                    </span>
                    <a href="<?php echo $endpoint['url'] ?>">
                        <?php echo $endpoint['path'] ?>
                    </a>
                </div>
            </div>

            <div class="list-group-item d-flex justify-content-between align-items-start">
                <p><?php echo $endpoint['description'] ?></p>
            </div>

            <?php if ($endpoint['params'] !== false): ?>
            <div class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Parameters</div>
                    <table class="table table-bordered">
                        <tbody>
                            <?php foreach ($endpoint['params'] as $key => $value): ?>
                            <tr>
                                <td><?php echo $key ?></td>
                                <td><?php echo $value ?></td>
                            </tr>
                            <?php endforeach?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endif?>

            <?php if ($endpoint['body'] !== false): ?>
            <div class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Request Body</div>
                    <pre>
                        <code>
<?php echo json_encode($endpoint['body'], JSON_PRETTY_PRINT) ?>
                        </code>
                    </pre>
                </div>
            </div>
            <?php endif?>

            <?php if ($endpoint['resp'] !== false): ?>
            <div class="list-group-item d-flex justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Response Body</div>
                    <pre>
                        <code>
<?php echo json_encode($endpoint['resp'], JSON_PRETTY_PRINT) ?>
                        </code>
                    </pre>
                </div>
            </div>
            <?php endif?>
        </div>
        <?php endforeach?>
    </section>
</div>
</body>
</html>
